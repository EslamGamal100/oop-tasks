/* This code is submitted to Dr.Mahmoud Ismail and Eng.Mohamed El-bialy
  * in the OOP course.
  * Subject:pint Repeated elements in an array of integres
  * Author:Eslam Gamal Thabet
  * Section:1, Bench No:6                                            
  * In any function the whole idea is depending on and a temporary array
  * which counts for elements scanned such that if an element been added 
  * the temp will be incremented by 1 and then when we scan it again
  * (in case of repetition) it will check if the temp is one or not if 
  * true then print, if exceeds one or not equals one it means it has been
  * already repeated so ignore it.                                                                               */

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class RepeatedElements {
	void printRepeatedElementsNlogN(int [] input_list){
		if(input_list ==  null){
			return;
		}
		
		Arrays.sort(input_list);
		int[] temp =new int [input_list.length];
		for(int i=0; i<input_list.length; i++){
			if(temp[input_list[i]] == 1){
				System.out.println(input_list[i]);
				temp[input_list[i]]++;
			}else {temp[input_list[i]]++ ;}
			
		}
		
	}
	
	void printRepeatedElementsNsquare(int[] input_list){
		if(input_list ==  null){
			return;
		}
		int[] temp =new int [input_list.length];
		for(int i=0; i<input_list.length; i++){
			for(int j=i; j<input_list.length; j++){
				if(input_list[i] == input_list[j] && j != i){
					temp[input_list[i]]++;
					if (temp[input_list[i]] == 1){
						System.out.println(input_list[i]);
					}
					else {continue;}
				}
			}
		}
	}
	
	void printRepeatedElementsN(int[] input_list){
		if(input_list == null){
			return;
		}
		int[] temp =new int [input_list.length];
		HashMap<Integer, Integer> myhash = new HashMap<Integer, Integer>();
		for(int i=0; i<input_list.length;i++){
			if(myhash.containsKey(input_list[i])){
				if(temp[input_list[i]] == 1){
					System.out.println(input_list[i]);
					temp[input_list[i]]++;
				}
				
			}else{
				myhash.put(input_list[i], null);
				temp[input_list[i]]++;
			}
		}
	}
		
	
	//main function with a switch case for testing all the functions
	public static void main(String[] args){
		int[] test_input = {4,5,5,5,1,2,8,4,3,1,3};
		RepeatedElements test = new RepeatedElements();
		//test.printRepeatedElementsN(test_input);
		Scanner scan = new Scanner(System.in);
		int OP_number;
		do{
			System.out.println("Enter Operation number:");
			OP_number = scan.nextInt();
			scan.nextLine();
			switch(OP_number){
			
			case 1:
				test.printRepeatedElementsNsquare(test_input);
				break;
			case 2:
				test.printRepeatedElementsNlogN(test_input);
				break;
			case 3:
				test.printRepeatedElementsN(test_input);
				break;
			case 0: System.out.println("GOODBYE!!");
			break;
			}
		}while(OP_number != 0);
	}
}
	
	
		

